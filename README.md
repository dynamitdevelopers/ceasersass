# Ceasersass

* A SASS port of Jared Hardy’s Ceaser Compass extension.*

## Documentation

For full documentation, visit [github.com](https://github.com/jhardy/compass-ceaser-easing).

## Further reading

[Ceaser CSS Easing Animation Tool](https://matthewlein.com/tools/ceaser)
